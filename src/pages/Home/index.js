import React, { Component } from "react";
import { Link } from "react-router-dom";
import firebase from "../../Firebase";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: "",
      sobrenome: "",
      dataNascimento: "",
    };
  }

  async componentDidMount() {
    firebase.auth().onAuthStateChanged(async (usuario) => {
      if (usuario) {
        var uid = usuario.uid;

        await firebase
          .firestore()
          .collection("usuario")
          .doc(uid)
          .get()
          .then((retorno) => {
            this.setState({
              nome: retorno.data().nome,
              sobrenome: retorno.data().sobrenome,
              dataNascimento: retorno.data().dataNascimento,
            });
          });
      }
    });
  }

  render() {
    return (
      <div className="container d-flex justify-content-center align-items-center vh-100">
        <div className="card">
          <div className="card-header">
            <h1 className="text-center">Bem-vindo(a), {this.state.nome}</h1>
          </div>
          <div className="card-body">
            <div className="form-group">
              <p>Nome: {this.state.nome}</p>
            </div>
            <div className="form-group">
              <p>Sobrenome: {this.state.sobrenome}</p>
            </div>
            <div className="form-group">
              <p>Data de Nascimento: {this.state.dataNascimento}</p>
            </div>
            <div className="text-center mt-3">
              <Link to="/" className="btn btn-danger btn-block">
                Logout
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Home;
