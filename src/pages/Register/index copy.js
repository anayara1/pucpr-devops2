import React, { Component } from "react"
import { Link } from "react-router-dom"
import firebase from '../../Firebase';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            sobrenome: "",
            email: "",
            senha: "",
            dataNascimento: ""
        }

        this.logar = this.logar.bind(this);

    }

    async logar() {
        await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
            .then(() => {
                window.location.href = '/Home'
            })
            .catch(() => {
                this.setState({ msg: <div className="text-center alert alert-danger mt-2">Usuário não cadastrado</div>});
            })
    }

    render() {
        return (
            <div className="container d-flex justify-content-center align-items-center vh-100">
                <div className="card">
                    <div className="card-header">
                        <h1 className="text-center">Login</h1>
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <input
                                type='email'
                                className='form-control'
                                placeholder='E-mail'
                                onChange={(e) => this.setState({ email: e.target.value })}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type='password'
                                className='form-control'
                                placeholder='Senha'
                                onChange={(e) => this.setState({ senha: e.target.value })}
                            />
                        </div>
                        <div className="text-center mt-3">
                            <button onClick={this.logar} className="btn btn-primary btn-block">Acessar</button>
                        </div>
                    </div>
                    <div className="card-footer text-center">
                        <Link to="/">Cadastrar</Link>
                        
                    </div>
                    { this.state.msg }
                </div>
            </div>

        )
    }

}
export default Login