import React, { Component } from "react"
import { Link } from "react-router-dom"
import firebase from '../../Firebase';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            dataNascimento: ""
        }

        this.Registrar = this.Registrar.bind(this);

    }

    async Registrar() {

        var RegisterSuccess = 0;

        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
            .then(async (retorno) => {
                await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                    nome: this.state.nome,
                    sobrenome: this.state.sobrenome,
                    dataNascimento: this.state.dataNascimento
                })

                RegisterSuccess = 1

                if (RegisterSuccess === 1) {
                    window.location.href = '/Home'
                    RegisterSuccess = 0
                }
            })
            .catch((error) => {
                console.log(error)
            });
    }

    render() {
        return (

            <div className="container d-flex justify-content-center align-items-center vh-100">
                <div className="card">
                    <div className="card-header">
                        <h1 className="text-center">Registro</h1>
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <input
                                type='text'
                                className='form-control'
                                placeholder='Nome'
                                onChange={(e) => this.setState({ nome: e.target.value })}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type='text'
                                className='form-control'
                                placeholder='Sobrenome'
                                onChange={(e) => this.setState({ sobrenome: e.target.value })}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type='email'
                                className='form-control'
                                placeholder='E-mail'
                                onChange={(e) => this.setState({ email: e.target.value })}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type='password'
                                className='form-control'
                                placeholder='Senha'
                                onChange={(e) => this.setState({ senha: e.target.value })}
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type='date'
                                className='form-control'
                                placeholder='Data de Nascimento'
                                onChange={(e) => this.setState({ dataNascimento: e.target.value })}
                            />
                        </div>
                        <div className="text-center mt-3">

                            <button onClick={this.Registrar} className="btn btn-primary btn-block">Cadastrar</button>

                        </div>
                    </div>

                    <div className="card-footer text-center">
                        <Link to="/Login">Login</Link>
                    </div>
                </div>
            </div>

        )
    }

}

export default Register