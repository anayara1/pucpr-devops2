import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';


const Rotas = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route exact={true} path="/" Component={Register} />
                <Route exact={true} path="/Login" Component={Login} />
                <Route exact={true} path="/Home" Component={Home} />
            </Routes>
        </BrowserRouter>
    )
}

export default Rotas;


